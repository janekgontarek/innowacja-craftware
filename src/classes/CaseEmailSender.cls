public class CaseEmailSender {
    @future
    public static void sendEmailToRestaurantWithCase(List<id> casesId){
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        List<Case> cases = [select id, ContactEmail, Subject, Description from Case where id in :casesId];
        System.debug(cases);
        for(Case currCase : cases){
            if(currCase.ContactEmail != null){
                List<String> toAddresses = new List<String>();
                System.debug(currCase.ContactEmail);
                toAddresses.add(currCase.ContactEmail);
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();              
                mail.setToAddresses(toAddresses);
                mail.setReplyTo('feedme8@feedme.com');
                mail.setSenderDisplayName('FeedMe8');
                mail.setSubject('New Case Created By: ');
                mail.setBccSender(false);
                mail.setUseSignature(false);
                mail.setPlainTextBody('Your Case related to Product: ' + ' has been created by '
                        + '\nSubject: ' + currCase.Subject
                        +' \nDescription: ' + currCase.Description);
//                mail.setHtmlBody('Your case:<b> ' + currCase.Id +' </b>has been created<p>');
                mails.add(mail);
            }
        }
        Messaging.sendEmail(mails);
        System.debug('Email send');
    }
}