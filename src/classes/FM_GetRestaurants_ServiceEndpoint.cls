/**
 * Created by janek on 21.12.2021.
 */

public with sharing class FM_GetRestaurants_ServiceEndpoint {
    @Future(Callout=true)
    public static void getRestaurants() {
        String url = 'https://test.salesforce.com/services/oauth2/token';

        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setHeader('Content-Type', HttpFormBuilder.GetContentType());
        req.setEndpoint(url);
        req.setMethod('POST');

        String body = '';
        body += HttpFormBuilder.WriteBoundary();
        body += HttpFormBuilder.WriteBodyParameter('grant_type', 'password');
        body += HttpFormBuilder.WriteBoundary();
        body += HttpFormBuilder.WriteBodyParameter('client_id', '3MVG9od6vNol.eBhZnbxPWOo3NeVAkfcUFtgnrCWeswdD17f88ajFmxYYuN5TXduf.TsHigO.V7zQTsqmkn9G');
        body += HttpFormBuilder.WriteBoundary();
        body += HttpFormBuilder.WriteBodyParameter('client_secret', '7A7A55E6CCBF867F80BBEB804378E2D6863E5539832C9B347733C4C5F4E05C53');
        body += HttpFormBuilder.WriteBoundary();
        body += HttpFormBuilder.WriteBodyParameter('username', 'integrationuser@feedme.com.feedme1');
        body += HttpFormBuilder.WriteBoundary();
        body += HttpFormBuilder.WriteBodyParameter('password', 'feedmepass123!');
        body += HttpFormBuilder.WriteBoundary(HttpFormBuilder.EndingType.CrLf);

        Blob formBlob = EncodingUtil.base64Decode(body);

        req.setHeader('Content-Length', String.valueOf(formBlob.size()));
        req.setBodyAsBlob(formBlob);

        HttpResponse res = h.send(req);

        String authorization = '';

        if(res.getStatusCode() == 200){
            Map<String, Object> response = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
            authorization += response.get('token_type');
            authorization += ' ';
            authorization += response.get('access_token');
        }
        else{
            System.debug('Couldn\'t get token');
            return;
        }

        req = new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint('https://feedme--feedme1.my.salesforce.com/services/apexrest/Restaurant');
        req.setHeader('Authorization', authorization);
        res = h.send(req);

        if(res.getStatusCode() != 200){
            System.debug('Couldn\'t get restaurants');
            return;
        }

//        Account[] accToDelete = [SELECT Id, Name FROM Account WHERE Name NOT IN ('Taco Bell', 'szop')];
//        delete accToDelete;

//        Opening_Hours__c[] ohToDelete = [SELECT Id, Name FROM Opening_Hours__c WHERE Name NOT IN ('OH-0000003', 'OH-0000004')];
//        delete ohToDelete;

        List<Object> restaurantsFromService = new List<Object>();
        restaurantsFromService.addAll((List<Object>)JSON.deserializeUntyped(res.getBody()));

        System.debug('Restaurant count from service: ' + restaurantsFromService.size());

        if(restaurantsFromService.size() < 1){
            System.debug('No restaurants to update/add');
            return;
        }

        List<String> serviceIds = new List<String>();
        List<String> serviceIdsOfRestaurantsToAdd = new List<String>();

        for(Object restaurant : restaurantsFromService) {
            Map<String, Object> restaurantMap = ((Map<String, Object>) restaurant);
            serviceIds.add((String)restaurantMap.get('restaurantId'));
        }

        serviceIdsOfRestaurantsToAdd.addAll(serviceIds);

        List<Account> restaurantsToUpdate = [SELECT Name, BillingCity, BillingCountry, BillingPostalCode, BillingStreet, Phone, Website, Service_Id__c
                                             FROM Account
                                             WHERE Service_Id__c IN :serviceIds];

        Map<String, Account> restaurantFromServiceId = new Map<String, Account>();
        for(Account acc : restaurantsToUpdate){
            restaurantFromServiceId.put(acc.Service_Id__c, acc);
            serviceIdsOfRestaurantsToAdd.remove(serviceIdsOfRestaurantsToAdd.indexOf(acc.Service_Id__c));
        }

        Integer restaurantsToInsertCount = restaurantsFromService.size() - restaurantsToUpdate.size();
        System.debug('Restaurants to insert: ' + restaurantsToInsertCount);
        System.debug('Restaurants to update: ' + restaurantsToUpdate.size());

        List<Account> restaurantsToInsert = new List<Account>();

        for(String serviceId : serviceIdsOfRestaurantsToAdd){
            Account acc = new Account(Name = 'temp', Service_Id__c = serviceId);
            restaurantsToInsert.add(acc);
            restaurantFromServiceId.put(serviceId, acc);
        }

        insert restaurantsToInsert;

        restaurantsToUpdate = [SELECT Id FROM Account WHERE Service_Id__c IN :serviceIds];
        List<Id> restaurantsToUpdateIds = new List<Id>();

        for(Account acc : restaurantsToUpdate){
            restaurantsToUpdateIds.add(acc.Id);
        }

        List<Opening_Hours__c> openingHoursToUpdate = [SELECT Id, Account__c FROM Opening_Hours__c WHERE Account__c IN :restaurantsToUpdateIds];
        Map<Id, Opening_Hours__c> openingHoursMapFromRestaurantIds = new Map<Id, Opening_Hours__c>();

        for(Opening_Hours__c openingHours : openingHoursToUpdate){
            openingHoursMapFromRestaurantIds.put(openingHours.Account__c, openingHours);
        }

        System.debug('OpeningHours to insert: ' + (restaurantsFromService.size() - openingHoursMapFromRestaurantIds.size()));
        System.debug('OpeningHours to update: ' + openingHoursMapFromRestaurantIds.size());

        for(Object restaurant : restaurantsFromService){
            Map<String, Object> restaurantMap = ((Map<String, Object>) restaurant);
            Account updateRestaurant = restaurantFromServiceId.get((String)restaurantMap.get('restaurantId'));

            Opening_Hours__c updateOpeningHours = openingHoursMapFromRestaurantIds.get(updateRestaurant.Id);
            if(updateOpeningHours == null){
                updateOpeningHours = new Opening_Hours__c(Account__c = updateRestaurant.Id);
                openingHoursToUpdate.add(updateOpeningHours);
            }

            updateRestaurant.Name = (String) restaurantMap.get('restaurantName');
            updateRestaurant.BillingStreet = (String) restaurantMap.get('street');
            updateRestaurant.BillingPostalCode = (String) restaurantMap.get('postalCode');
            updateRestaurant.Phone = (String) restaurantMap.get('phoneNumber');
            updateRestaurant.BillingCountry = (String) restaurantMap.get('country');
            updateRestaurant.BillingCity = (String) restaurantMap.get('city');
            updateRestaurant.Website = (String) restaurantMap.get('website');
            updateRestaurant.Service_Id__c = (String) restaurantMap.get('restaurantId');

            Map<String, Object> openingHoursMap = (Map<String, Object>)restaurantMap.get('openingHours');

            Map<String, Time> timeMap = StringToTimeGenerator.generate(openingHoursMap);

            updateOpeningHours.Monday_Start_Time__c = timeMap.get('Monday_Start');
            updateOpeningHours.Monday_End_Time__c = timeMap.get('Monday_End');
            updateOpeningHours.Tuesday_Start_Time__c = timeMap.get('Tuesday_Start');
            updateOpeningHours.Tuesday_End_Time__c = timeMap.get('Tuesday_End');
            updateOpeningHours.Wednesday_Start_Time__c = timeMap.get('Wednesday_Start');
            updateOpeningHours.Wednesday_End_Time__c = timeMap.get('Wednesday_End');
            updateOpeningHours.Thursday_Start_Time__c = timeMap.get('Thursday_Start');
            updateOpeningHours.Thursday_End_Time__c = timeMap.get('ThursdayThursday_End');
            updateOpeningHours.Friday_Start_Time__c = timeMap.get('Friday_Start');
            updateOpeningHours.Friday_End_Time__c = timeMap.get('Friday_End');
            updateOpeningHours.Saturday_Start_Time__c = timeMap.get('Saturday_Start');
            updateOpeningHours.Saturday_End_Time__c = timeMap.get('Saturday_End');
            updateOpeningHours.Sunday_Start_Time__c = timeMap.get('Sunday_Start');
            updateOpeningHours.Sunday_End_Time__c = timeMap.get('Sunday_End');
            updateOpeningHours.Account__c = updateRestaurant.Id;
        }

        update restaurantFromServiceId.values();
        upsert openingHoursToUpdate;
    }
}