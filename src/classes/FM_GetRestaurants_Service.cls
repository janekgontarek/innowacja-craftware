/**
 * Created by janek on 21.12.2021.
 */

global with sharing class FM_GetRestaurants_Service implements Schedulable {

    global void execute(SchedulableContext sc) {
        FM_GetRestaurants_ServiceEndpoint.getRestaurants();
    }
}