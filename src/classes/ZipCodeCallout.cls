public class ZipCodeCallout {
	@future(callout=true)
    public static void makeCalloutForProvince(Map<Id, String> addressesWithZipCodes) {
        Set<Id> idSet = addressesWithZipCodes.keySet();
        List<Address__c> addressesToUpdate = [select Province__c from Address__c where Address__c.Id in :idSet];
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setMethod('GET');
        request.setHeader('Accept', 'application/json');
        for(Address__c currentAddress : addressesToUpdate){
            request.setEndpoint('http://kodpocztowy.intami.pl/api/' + addressesWithZipCodes.get(currentAddress.Id));
            HttpResponse response = http.send(request);
            if(response.getStatusCode() == 200) {
                System.debug(response.getBody());
                List<Object> results = (List<Object>) JSON.deserializeUntyped(response.getBody());
                String province = ((Map<String, Object>)(results[0])).get('wojewodztwo').toString();
                System.debug('Received the following province: ' + province);
                currentAddress.Province__c = province;
            }
            else{
                System.debug('Status code != 200');
                currentAddress.Province__c = '';
            } 
        }
        update addressesToUpdate;
    }
}