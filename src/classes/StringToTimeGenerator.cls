/**
 * Created by janek on 06.01.2022.
 */

public with sharing class StringToTimeGenerator {
    public static Map<String, Time> generate(Map<String, Object> openingHours){
        List<String> days = new List<String>{'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'};
        String start = '_Start';
        String endStr = '_End';

        Map<String, Time> res = new Map<String, Time>();

        for(String day : days){
            List<String> hours = ((String)openingHours.get(day)).split(' - ');
            List<String> hoursMinutes = hours.get(0).split(':');
            res.put(day + start, Time.newInstance(Integer.valueOf(hoursMinutes[0])
                    ,Integer.valueOf(hoursMinutes[1])
                    ,0
                    ,0));

            hoursMinutes = hours.get(1).split(':');
            res.put(day + endStr, Time.newInstance(Integer.valueOf(hoursMinutes[0])
                    ,Integer.valueOf(hoursMinutes[1])
                    ,0
                    ,0));
        }

        return res;
    }
}