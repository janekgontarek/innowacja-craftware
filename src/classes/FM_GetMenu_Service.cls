/**
 * Created by janek on 12.01.2022.
 */

global with sharing class FM_GetMenu_Service implements Schedulable {

    global void execute(SchedulableContext sc) {
        FM_GetMenu_ServiceEndpoint.getMenu();
    }
}