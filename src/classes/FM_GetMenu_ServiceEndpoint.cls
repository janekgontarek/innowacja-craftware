/**
 * Created by janek on 12.01.2022.
 */

public with sharing class FM_GetMenu_ServiceEndpoint {
    @Future(Callout=true)
    public static void getMenu() {
        String url = 'https://test.salesforce.com/services/oauth2/token';

        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setHeader('Content-Type', HttpFormBuilder.GetContentType());
        req.setEndpoint(url);
        req.setMethod('POST');

        String body = '';
        body += HttpFormBuilder.WriteBoundary();
        body += HttpFormBuilder.WriteBodyParameter('grant_type', 'password');
        body += HttpFormBuilder.WriteBoundary();
        body += HttpFormBuilder.WriteBodyParameter('client_id', '3MVG9od6vNol.eBhZnbxPWOo3NeVAkfcUFtgnrCWeswdD17f88ajFmxYYuN5TXduf.TsHigO.V7zQTsqmkn9G');
        body += HttpFormBuilder.WriteBoundary();
        body += HttpFormBuilder.WriteBodyParameter('client_secret', '7A7A55E6CCBF867F80BBEB804378E2D6863E5539832C9B347733C4C5F4E05C53');
        body += HttpFormBuilder.WriteBoundary();
        body += HttpFormBuilder.WriteBodyParameter('username', 'integrationuser@feedme.com.feedme1');
        body += HttpFormBuilder.WriteBoundary();
        body += HttpFormBuilder.WriteBodyParameter('password', 'feedmepass123!');
        body += HttpFormBuilder.WriteBoundary(HttpFormBuilder.EndingType.CrLf);

        Blob formBlob = EncodingUtil.base64Decode(body);

        req.setHeader('Content-Length', String.valueOf(formBlob.size()));
        req.setBodyAsBlob(formBlob);

        HttpResponse res = h.send(req);

        String authorization = '';

        if(res.getStatusCode() == 200){
            Map<String, Object> response = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
            authorization += response.get('token_type');
            authorization += ' ';
            authorization += response.get('access_token');
        }
        else{
            System.debug('Couldn\'t get token');
            return;
        }

        //**********************************************************//

        List<Account> restaurantsToUpdateMenu = [SELECT Id, Name, Service_Id__c
                                                 FROM Account
                                                 WHERE Service_Id__c != NULL];

        Map<String, List<Id>> bodyMap = new Map<String, List<Id>>();

        List<Id> ids = new List<Id>();
        Map<String, Id> accountIdFromServiceId = new Map<String, Id>();
        for(Account acc : restaurantsToUpdateMenu){
            ids.add(acc.Service_Id__c);
            accountIdFromServiceId.put(acc.Service_Id__c, acc.Id);
        }

        bodyMap.put('restaurantIds', ids);

        req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint('https://feedme--feedme1.my.salesforce.com/services/apexrest/Menu');
        req.setHeader('Authorization', authorization);
        req.setHeader('Content-Type', 'application/json');
        req.setBody(JSON.serialize(bodyMap));

        res = h.send(req);

        System.debug(res.getBody());
        System.debug(req.getBody());

        if(res.getStatusCode() != 200){
            System.debug('Couldn\'t get menu');
            return;
        }
        else{
            System.debug('Success');
        }

        List<Object> products = (List<Object>)JSON.deserializeUntyped(res.getBody());

        System.debug(products.size());

        List<Product2> productsToAdd = new List<Product2>();
        List<String> menuItemIds = new List<String>();

        Map<String, Product2> productsFromServiceMap = new Map<String, Product2>();

        for(Object product : products){
            Product2 addProduct = new Product2();
            Map<String, Object> productMap = (Map<String, Object>)product;

            addProduct.Name = (String)productMap.get('menuItemName');
            addProduct.Description = (String)productMap.get('itemDescription');
            addProduct.Category__c = (String)productMap.get('category');
            addProduct.Sub_Category__c = (String)productMap.get('subcategory');
            addProduct.Price__c = (Double)productMap.get('price');
            addProduct.Account__c = (String)accountIdFromServiceId.get((String)productMap.get('restaurantId'));
            addProduct.Menu_Item_Id__c = (String)productMap.get('menuItemId');

            menuItemIds.add(addProduct.Menu_Item_Id__c);
            productsFromServiceMap.put(addProduct.Menu_Item_Id__c, addProduct);
            productsToAdd.add(addProduct);
        }

        List<Product2> productsToUpdate = [SELECT Name, Description, Category__c, Sub_Category__c, Account__c, Id, Menu_Item_Id__c
                                           FROM Product2
                                           WHERE Menu_Item_Id__c IN :menuItemIds];

        for(Product2 product : productsToUpdate){
            product.Name = productsFromServiceMap.get(product.Menu_Item_Id__c).Name;
            product.Description = productsFromServiceMap.get(product.Menu_Item_Id__c).Description;
            product.Category__c = productsFromServiceMap.get(product.Menu_Item_Id__c).Category__c;
            product.Sub_Category__c = productsFromServiceMap.get(product.Menu_Item_Id__c).Sub_Category__c;
            product.Price__c = productsFromServiceMap.get(product.Menu_Item_Id__c).Price__c;
            product.Account__c = productsFromServiceMap.get(product.Menu_Item_Id__c).Account__c;

            productsFromServiceMap.remove(product.Menu_Item_Id__c);
        }

        System.debug(productsToUpdate);
        System.debug(productsFromServiceMap.values());

        productsToAdd = productsFromServiceMap.values();

        insert productsToAdd;
        update productsToUpdate;
    }
}