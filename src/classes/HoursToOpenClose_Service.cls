/**
 * Created by janek on 15.12.2021.
 */

public with sharing class HoursToOpenClose_Service {
    @AuraEnabled(cacheable=true)
    public static Map<String, Map<String, String>> getOpeningHoursAndNameForAccountFromOrder(Id orderId) {
        Map<String, Map<String, String>> result;
        Order order = [SELECT AccountId FROM Order where Id = :orderId LIMIT 1];
        Account restaurant = [SELECT Id, Name FROM Account WHERE Id = :order.AccountId LIMIT 1];
        Opening_Hours__c oh = [
                SELECT Account__c, Monday_Start_Time__c, Monday_End_Time__c, Tuesday_Start_Time__c, Tuesday_End_Time__c,
                        Wednesday_Start_Time__c, Wednesday_End_Time__c, Thursday_Start_Time__c, Thursday_End_Time__c,
                        Friday_Start_Time__c, Friday_End_Time__c, Saturday_Start_Time__c, Saturday_End_Time__c, Sunday_Start_Time__c, Sunday_End_Time__c
                FROM Opening_Hours__c
                WHERE Account__c = :restaurant.Id
        ];

        if (oh != null) {
            result = new Map<String, Map<String, String>>();
            result.put('Monday', new Map<String, String>{
                    'Start' => getOpenHoursString(oh.Monday_Start_Time__c), 'End' => getCloseHoursString(oh.Monday_End_Time__c)
            });
            result.put('Tuesday', new Map<String, String>{
                    'Start' => getOpenHoursString(oh.Tuesday_Start_Time__c), 'End' => getCloseHoursString(oh.Tuesday_End_Time__c)
            });
            result.put('Wednesday', new Map<String, String>{
                    'Start' => getOpenHoursString(oh.Wednesday_Start_Time__c), 'End' => getCloseHoursString(oh.Wednesday_End_Time__c)
            });
            result.put('Thursday', new Map<String, String>{
                    'Start' => getOpenHoursString(oh.Thursday_Start_Time__c), 'End' => getCloseHoursString(oh.Thursday_End_Time__c)
            });
            result.put('Friday', new Map<String, String>{
                    'Start' => getOpenHoursString(oh.Friday_Start_Time__c), 'End' => getCloseHoursString(oh.Friday_End_Time__c)
            });
            result.put('Saturday', new Map<String, String>{
                    'Start' => getOpenHoursString(oh.Saturday_Start_Time__c), 'End' => getCloseHoursString(oh.Saturday_End_Time__c)
            });
            result.put('Sunday', new Map<String, String>{
                    'Start' => getOpenHoursString(oh.Sunday_Start_Time__c), 'End' => getCloseHoursString(oh.Sunday_End_Time__c)
            });
            result.put('Name', new Map<String, String>{'Name' => restaurant.Name, 'Name' => restaurant.Name});
        }
        return result;
    }

    public static String getOpenHoursString(Time startTime) {
        Datetime startDate = Datetime.newInstance(System.now().date(), startTime);

        return startDate.format('HH:ss');
    }

    public static String getCloseHoursString(Time endTime) {
        Datetime endDate = Datetime.newInstance(System.now().date(), endTime);

        return endDate.format('HH:ss');
    }
}