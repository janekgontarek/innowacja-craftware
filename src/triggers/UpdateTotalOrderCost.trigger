trigger UpdateTotalOrderCost on Order_Product__c (after insert,after update) {
    Map<Id, Product2> products = new Map<Id, Product2>([SELECT Id,Price__c FROM Product2]);
    Map<Id, Order> orders = new Map<Id, Order>([SELECT Id,Total_Order_Cost__c,HasDiscount__c,AccountId FROM Order]);

    List<Loyalty_Stamp__c>stampList=[SELECT Id,Account__c,Discount__c FROM Loyalty_Stamp__c];

    List<Order>ordersToUpdate=new List<Order>();
	for(Order_Product__c o:Trigger.new)
    {
        Decimal totalOrderCost= 0;
        Product2 product=products.get(o.Product__c);
        totalOrderCost+=product.Price__c;
        totalOrderCost*=o.Quantity__c;

        Order order=orders.get(o.Order__c);
        if(order.Total_Order_Cost__c==null)order.Total_Order_Cost__c=0;
        if(order.HasDiscount__c){
            for( Loyalty_Stamp__c stamp:stampList)
            {
                if(stamp.Account__c==order.AccountId)
                {
                    order.Total_Order_Cost__c=(order.Total_Order_Cost__c*100)/stamp.Discount__c;
                    order.Total_Order_Cost__c+=totalOrderCost;
                    order.Total_Order_Cost__c=order.Total_Order_Cost__c*(stamp.Discount__c/100);
                }
            }
        }
        else{
            order.Total_Order_Cost__c+=totalOrderCost;
        }
        ordersToUpdate.add(order);
    }
    update(ordersToUpdate);
}