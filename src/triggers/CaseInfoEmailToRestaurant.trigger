trigger CaseInfoEmailToRestaurant on Case (after insert, after update) {
    List<Id> casesToSendEmail = new List<Id>();
    for(Case currCase : Trigger.New){
    	casesToSendEmail.add(currCase.Id);
    }
    CaseEmailSender.sendEmailToRestaurantWithCase(casesToSendEmail);
   	System.debug(casesToSendEmail);
}