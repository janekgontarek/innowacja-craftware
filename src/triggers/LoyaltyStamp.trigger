trigger LoyaltyStamp on Order (before insert) {

  List<Account> accounts=[SELECT Id FROM Account];
  List<User> clients=[SELECT Id FROM User];

  Integer ordersCounter=[SELECT COUNT() FROM Order WHERE OwnerId=:clients AND AccountId=:accounts];
    for(Order order:Trigger.new){
      if(Math.mod(ordersCounter+1,3)==0 && ordersCounter!=0){
        order.HasDiscount__c=true;
      }
      ordersCounter++;
    }
}