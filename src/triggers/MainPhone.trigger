trigger MainPhone on Contact (before insert, before update) {
    for(Contact c : Trigger.New) {
        if(c.Main_Phone__c == 'Business Phone'){
            c.Phone = c.Business_Phone__c;
        }
        else{
            c.Phone = c.Private_Phone__c;
        }
    }
}