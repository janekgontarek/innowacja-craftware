trigger OrderSecure on Order (before insert, before update) 
{
    List <Opening_Hours__c> hourList = [SELECT Id, Account__c,Monday_Start_Time__c, Monday_End_Time__c, Tuesday_Start_Time__c, Tuesday_End_Time__c,
                            Wednesday_Start_Time__c, Wednesday_End_Time__c, Thursday_Start_Time__c, Thursday_End_Time__c,
                            Friday_Start_Time__c, Friday_End_Time__c, Saturday_Start_Time__c, Saturday_End_Time__c, Sunday_Start_Time__c, Sunday_End_Time__c
                         	FROM Opening_Hours__c ];

    Map<Id,Opening_Hours__c> hourMap = new Map<Id, Opening_Hours__c>();
    for( Opening_Hours__c a: hourList)
    {
        hourMap.put(a.Account__c,a);
    }

    Date d = System.today();
    DateTime ss = DateTime.now();
	Datetime dt = (DateTime)d;
	String dayOfWeek = dt.format('EEEE');
         
	for( Order i: Trigger.New)
    {
        switch on dayOfWeek
        {
            when 'Monday'
            {
                if( hourMap.get(i.AccountId).Monday_Start_Time__c > ss.time() || hourMap.get(i.AccountId).Monday_End_Time__c < ss.time())
                    i.addError('Cannot be ordered at this time');
            }
            when 'Tuesday'
            {
                if(hourMap.get(i.AccountId).Tuesday_Start_Time__c > ss.time() || hourMap.get(i.AccountId).Tuesday_End_Time__c < ss.time())
                    i.addError('Cannot be ordered at this time');

            }
            when 'Wednesday'
            {
                if(hourMap.get(i.AccountId).Wednesday_Start_Time__c > ss.time() || hourMap.get(i.AccountId).Wednesday_End_Time__c < ss.time())
                    i.addError('Cannot be ordered at this time');
            }
            when 'Thursday'
            {
                if(hourMap.get(i.AccountId).Thursday_Start_Time__c > ss.time() || hourMap.get(i.AccountId).Thursday_End_Time__c < ss.time())
                    i.addError('Cannot be ordered at this time');
            }
            when 'Friday'
            {
                if(hourMap.get(i.AccountId).Friday_Start_Time__c > ss.time() || hourMap.get(i.AccountId).Friday_End_Time__c < ss.time())
                    i.addError('Cannot be ordered at this time');
            }
            when 'Saturday'
            {
               if(hourMap.get(i.AccountId).Saturday_Start_Time__c > ss.time() || hourMap.get(i.AccountId).Saturday_End_Time__c < ss.time())
                    i.addError('Cannot be ordered at this time');
            }
            when 'Sunday'
            {
                if(hourMap.get(i.AccountId).Sunday_Start_Time__c > ss.time() || hourMap.get(i.AccountId).Sunday_End_Time__c < ss.time())
                    i.addError('Cannot be ordered at this time');

            }
            when else
            {
                i.addError('Cannot be ordered at this time');
            }
        }
    }
}