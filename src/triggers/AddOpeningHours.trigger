/**
 * Created by janek on 12.01.2022.
 */

trigger AddOpeningHours on Account (after insert, before delete) {

    if(Trigger.isInsert){
        List<Opening_Hours__c> openingHoursToInsert = new List<Opening_Hours__c>();
        for(Account acc : Trigger.New){
            Opening_Hours__c openingHours = new Opening_Hours__c(Account__c = acc.Id);
            openingHoursToInsert.add(openingHours);
        }

        insert openingHoursToInsert;
    }

    else if(Trigger.isDelete){
        List<Id> restaurantIds = new List<Id>();

        for(Account acc : Trigger.Old){
            restaurantIds.add(acc.Id);
        }
        List<Opening_Hours__c> openingHoursToDelete = [SELECT Id, Account__c
                                                       FROM Opening_Hours__c
                                                       WHERE Account__c IN :restaurantIds];
        delete openingHoursToDelete;
    }
}