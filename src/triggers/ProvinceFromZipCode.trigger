trigger ProvinceFromZipCode on Address__c (after insert) {
    Map<Id, String> addressesWithZipCodeToUpdate = new Map<Id, String>();
    for(Address__c address : Trigger.New) {
        String zipCode = address.Zip_Code__c;
        addressesWithZipCodeToUpdate.put(address.Id, zipCode);
    }
    ZipCodeCallout.makeCalloutForProvince(addressesWithZipCodeToUpdate);
}