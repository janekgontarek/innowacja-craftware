trigger FreeDelivery on Order (before insert, before update) {
    for(Order order : Trigger.New){
        if((50 - order.Total_Order_Cost__c)>0){
            order.Amount_To_Free_Delivery__c = (50 - order.Total_Order_Cost__c);
            order.Delivery_Amount__c = 20;
        }
        else{
            order.Amount_To_Free_Delivery__c = 0;
            order.Delivery_Amount__c = 0;
        }
    }
}