import { LightningElement, api, wire, track } from 'lwc';
import getOpeningHoursForAccount from '@salesforce/apex/FM_OpeningHours_ServiceEndpoint.getOpeningHoursForAccount';

const DAYS = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
const THEMES = {'otherDay' : 'slds-grid slds-gutters', 'today' : 'slds-grid slds-gutters slds-theme_inverse'};

function MapValue(classC, hours) {
    this.classC = classC;
    this.hours = hours;
}

export default class OpeningHours extends LightningElement {
    @track oHours = [];
    @api recordId;
    @track today = DAYS[new Date().getDay()];

    @wire(getOpeningHoursForAccount, {accountId: '$recordId'})
    wiredHours({ error, data }) {
            if (data) {

               for(let key in data) {
                   if (data.hasOwnProperty(key)) {
                       let obj;
                       if(this.today === key){
                           console.log(key);
                           obj = new MapValue(THEMES['today'], data[key]);
                       }
                       else{
                           console.log(key);
                           obj = new MapValue(THEMES['otherDay'], data[key]);
                       }
                       this.oHours.push({value: obj, key:key});
                   }
               }
            }
            else if (error) {
                console.log(error)
            }
    }
}