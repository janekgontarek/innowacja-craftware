import { getRecord } from 'lightning/uiRecordApi';
import { api, LightningElement, track, wire } from 'lwc';

const FIELDS = ['Account.BillingStreet', 'Account.BillingCity', 'Account.BillingCountry', 'Account.Name'];

export default class Map extends LightningElement {
    @api recordId;
    @track account;
    @track mapMarkers = [];
    @track zoomLevel = 18;
    @track center;
    @track mapOptions;
    @wire(getRecord, {recordId: '$recordId', fields: FIELDS})
    wiredAccount({data, error}) {
        if (data) {
            this.account = data;
            this.mapMarkers = [
                {
                    location: {
                        City: this.account.fields.BillingCity.value,
                        Country: this.account.fields.BillingCountry.value,
                        Street: this.account.fields.BillingStreet.value,
                    },
                    title: this.account.fields.Name.value,
                    icon: 'standard:account',
                },
            ];
            this.center = {
                location: {
                    City: this.account.fields.BillingCity.value,
                    Country: this.account.fields.BillingCountry.value,
                    Street: this.account.fields.BillingStreet.value,
                },
            };
            this.mapOptions = {
                draggable: false,
                disableDefaultUI: true,
                disableDoubleClickZoom: true,
                scrollwheel: false,
            };
        } else if (error) {
            console.log('Error');
            this.data = undefined;
        }
    }
}