/**
 * Created by janek on 15.12.2021.
 */

import {api, LightningElement, track, wire} from 'lwc';
import getOpeningHoursAndNameForAccountFromOrder from '@salesforce/apex/HoursToOpenClose_Service.getOpeningHoursAndNameForAccountFromOrder';

const DAYS = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

export default class HoursToOpenClose extends LightningElement {
    @api recordId;
    @track today = DAYS[new Date().getDay()];
    @track hour = new Date().getHours();
    @track minutes = new Date().getMinutes();
    @track now = new Date();
    @track countDownStartTime= new Date();
    @track countDownEndTime= new Date();
    @track title = "Opens in"
    @track body;
    @track startTime;
    @track endTime;

    @wire(getOpeningHoursAndNameForAccountFromOrder, {orderId: '$recordId'})
    wiredHours({ error, data }) {
        if (data) {
            if(data.hasOwnProperty(this.today)){
                this.startTime = (data[this.today]['Start']).split(':');
                this.endTime = (data[this.today]['End']).split(':');

                this.countDownStartTime.setHours(this.startTime[0], this.startTime[1]);
                this.countDownEndTime.setHours(this.endTime[0], this.endTime[1]);

                if(this.countDownEndTime < this.countDownStartTime){
                    this.countDownEndTime.setDate(this.countDownEndTime.getDate() + 1);
                }

                let distance;
                let title = data["Name"]["Name"] + " Closes In ";

                if(this.countDownStartTime <= this.now && this.countDownEndTime >= this.now){
                    distance = this.countDownEndTime - this.now;
                    if(distance < 0){
                        distance *= -1;
                    }
                    this.body = "Welcome!"
                }
                else{
                    distance = this.countDownStartTime - this.now;
                    if(distance < 0){
                        distance *= -1;
                    }
                    title = data["Name"]["Name"] + " Opens In ";
                    this.body = "See You Tomorrow!"

                }
                let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));

                this.title = title + hours + "h " + minutes + "m ";
            }
        }
        else if (error) {
            console.log(error)
        }
    }
}